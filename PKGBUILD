# Alpine Maintainer: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Clar Fon <them@lightdark.xyz>
# Contributor: Pellegrino Prevete <pellegrinoprevete@gmail.com>

pkgname="abuild"
pkgver=3.9.0
_ver=${pkgver%_git*}
pkgrel=1
pkgdesc="Script to build Alpine Packages"
url="https://git.alpinelinux.org/cgit/abuild/"
arch=("x86_64" "i686" "pentium4")
license=("GPL2")
makedepends=("zlib" "pkgconfig" "scdoc")
depends=("busybox" "glibc" "pax-utils" "openssl" "apk-tools" "attr" "tar" "pkgconf" "lzip" "curl"
"bubblewrap" "gettext" "git")
opt_depends=("perl: for cpan resolver"
             "perl-libwww: for cpan resolver"
             "perl-json: for cpan resolver"
             "perl-module-build-tiny: for cpan resolver"
             "perl-lwp-protocol-https: for cpan resolver"
             "ruby: for gem resolver"
             "ruby-augeas: for gem resolver")
pkggroups="abuild"
source=("https://git.alpinelinux.org/${pkgname}/snapshot/${pkgname}-${pkgver}.tar.gz")
sha512sums=('a3075b18d4a085ca796d1c2df703c3e7c80e682623175eb0822479f3a6d96ffba571f283bdec8ae3db832e296f9e58bdd0f58097b86b503a91fbb40148084a68')

prepare() {
	cd "$srcdir/$pkgname-$_ver"
	sed -i -e "/^CHOST=/s/=.*/=$CARCH/" abuild.conf
}

build() {
	cd "$srcdir/$pkgname-$_ver"
	make VERSION="$pkgver-r$pkgrel"
}

package() {
	cd "$srcdir/$pkgname-$_ver"
	make install VERSION="$pkgver-r$pkgrel" DESTDIR="$pkgdir"
	for bin in "$pkgdir"/usr/bin/*; do
		sed -e "1s|#!/bin/ash|#!/bin/busybox ash|" -i "$bin"
	done
	install -m 644 abuild.conf "$pkgdir"/etc/abuild.conf
	install -d -m 775 -o nobody -g nobody "$pkgdir"/var/cache/distfiles
}
